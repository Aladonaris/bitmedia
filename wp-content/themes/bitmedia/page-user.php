<?php
    get_header();
    global $wpdb;
    global $user_id;

$user_clicks_five = array();
$user_clicks_seven = array();
$user_clicks_all = array();

$user_views_five = array();
$user_views_seven = array();
$user_views_all = array();

$get_user = $wpdb->get_row("SELECT first_name, last_name FROM users_table WHERE user_id=$user_id");
$get_user_statistic = $wpdb->get_results("SELECT * FROM users_statistic WHERE user_id=$user_id");

//var_dump($get_user_statistic);
for($i=0; $i < 5; $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_clicks_five[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->clicks
    ];
}
for($i=0; $i < 7; $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_clicks_seven[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->clicks
    ];
}
for($i=0; $i < count($get_user_statistic); $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_clicks_all[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->clicks
    ];
}

for($i=0; $i < 5; $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_views_five[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->page_views
    ];
}

for($i=0; $i < 7; $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_views_seven[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->page_views
    ];
}

for($i=0; $i < count($get_user_statistic); $i++){
    $phpTimestamp = strtotime($get_user_statistic[$i]->date);
    $javaScriptTimestamp = $phpTimestamp * 1000;
    $user_views_all[] = [
        'x' => $javaScriptTimestamp,
        'y' => $get_user_statistic[$i]->page_views
    ];
}

?>
<main id="primary" class="site-main">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-site">
                    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-title">
                    <p><?php echo $get_user->first_name.' '.$get_user->last_name?></p>
                </div>
            </div>
        </div>
        <div class="user-stastic-block">
            <div class="row">
                <div class="col-6">
                    <div class="user-statistic">
                        <p>Clicks</p>
                    </div>
                </div>
                <div class="col-6">
                    Show data for:
                    <select id="select">
                        <option value="3">5 days</option>
                        <option value="7" selected>7 days</option>
                        <option value="all">all time</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
        </div>

        <div class="user-stastic-block">
            <div class="row">
                <div class="col-6">
                    <div class="user-statistic">
                        <p>Views</p>
                    </div>
                </div>
                <div class="col-6">
                    Show data for:
                    <select id="select_views">
                        <option value="3">5 days</option>
                        <option value="7" selected>7 days</option>
                        <option value="all">all time</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="viewsContainer" style="height: 370px; width: 100%;"></div>
            </div>
        </div>

        <script>
            ( function( $ ) {
                $(document).ready(function() {
                    var chart = new CanvasJS.Chart("chartContainer", {
                        animationEnabled: true,
                        title:{
                            text: ""
                        },
                        axisY: {
                            title: "",
                            valueFormatString: "#####",
                        },
                        data: [{
                            type: "spline",
                            markerSize: 5,
                            xValueFormatString: "YYYY-MM-DD",
                            yValueFormatString: "#####",
                            xValueType: "dateTime",
                            dataPoints: <?php echo json_encode($user_clicks_seven, JSON_NUMERIC_CHECK); ?>
                        }]
                    });

                    chart.render();

                    var views = new CanvasJS.Chart("viewsContainer", {
                        animationEnabled: true,
                        title:{
                            text: ""
                        },
                        axisY: {
                            title: "",
                            valueFormatString: "#####",
                        },
                        data: [{
                            type: "spline",
                            markerSize: 5,
                            xValueFormatString: "YYYY-MM-DD",
                            yValueFormatString: "#####",
                            xValueType: "dateTime",
                            dataPoints: <?php echo json_encode($user_views_seven, JSON_NUMERIC_CHECK); ?>
                        }]
                    });

                    views.render();

            $('#select').change(function(){
                var value = $(this).val();
                console.log(value);
                let sis = '';
                if(value == 3){ views = <?php echo json_encode($user_views_five, JSON_NUMERIC_CHECK); ?>}
                else if(value == 7){ views = <?php echo json_encode($user_views_seven, JSON_NUMERIC_CHECK); ?>}
                else {views = <?php echo json_encode($user_views_all, JSON_NUMERIC_CHECK); ?>}
                    var chart = new CanvasJS.Chart("chartContainer", {
                        animationEnabled: true,
                        title:{
                            text: ""
                        },
                        axisY: {
                            title: "",
                            valueFormatString: "#####",
                        },
                        data: [{
                            type: "spline",
                            markerSize: 5,
                            xValueFormatString: "YYYY-MM-DD",
                            yValueFormatString: "#####",
                            xValueType: "dateTime",
                            dataPoints: views
                        }]
                    });

                    chart.render();
            });

                    $('#select_views').change(function(){
                        var value = $(this).val();
                        console.log(value);
                        let viewsUser = '';
                        if(value == 3){ viewsUser = <?php echo json_encode($user_clicks_five, JSON_NUMERIC_CHECK); ?>}
                        else if(value == 7){ viewsUser = <?php echo json_encode($user_clicks_seven, JSON_NUMERIC_CHECK); ?>}
                        else {viewsUser = <?php echo json_encode($user_clicks_all, JSON_NUMERIC_CHECK); ?>}
                        var views = new CanvasJS.Chart("viewsContainer", {
                            animationEnabled: true,
                            title:{
                                text: ""
                            },
                            axisY: {
                                title: "",
                                valueFormatString: "#####",
                            },
                            data: [{
                                type: "spline",
                                markerSize: 5,
                                xValueFormatString: "YYYY-MM-DD",
                                yValueFormatString: "#####",
                                xValueType: "dateTime",
                                dataPoints: viewsUser
                            }]
                        });

                        views.render();
                    });
                })}( jQuery ) );

            //window.onload = function () {
            //
            //    var chart = new CanvasJS.Chart("chartContainer", {
            //        animationEnabled: true,
            //        title:{
            //            text: ""
            //        },
            //        axisY: {
            //            title: "",
            //            valueFormatString: "#####",
            //            suffix: "mn",
            //            prefix: "$"
            //        },
            //        data: [{
            //            type: "spline",
            //            markerSize: 5,
            //            xValueFormatString: "YYYY-MM-DD",
            //            yValueFormatString: "#####",
            //            xValueType: "dateTime",
            //            dataPoints: <?php //echo json_encode($users_clicks, JSON_NUMERIC_CHECK); ?>
            //        }]
            //    });
            //
            //    chart.render();
            //
            //}
        </script>
    </div>
</main><!-- #main -->
<?php
get_footer();
